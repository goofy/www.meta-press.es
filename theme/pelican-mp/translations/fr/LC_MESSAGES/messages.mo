��          <               \   a   ]   W   �   I     �  a  �     [   �  `   �   Discover millions of results within seconds, and explore the last ones in Firefox via this addon. Explore the press from your computer, with no middlemen between the newspapers and you. Select your press review and publish it in one click. Set email alerts… Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: siltaar@acoeuro.com
POT-Creation-Date: 2017-12-21 20:58+0100
PO-Revision-Date: 2017-12-21 20:59+0100
Last-Translator: Simon Descarpentries <siltaar@acoeuro.com>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
 Découvrez des millions de résultats en quelques secondes, et explorez les plus récents dans votre navigateur grâce à cet addon Firefox. Explorez la presse depuis votre ordinateur, sans intermédiaire entre vous et vos journaux. Sélectionnez votre revue de presse et publiez-la en un clic. Réglez vos alertes par courriels. 