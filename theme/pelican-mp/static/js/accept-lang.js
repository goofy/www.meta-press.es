// name   : accept-lang.js
// author : Simon Descarpentries, simon /\ acoeuro [] com
// date   : 2018-01
// licence: GPLv3
//
// jshint -W097
//

'use strict';

var default_lang = default_lang || 'en';
var page_lang = document.documentElement.lang;
var nav_lang = navigator.language.slice(0,2);

var dom_part = document.URL.split('/').slice(0,3).join('/');

if (page_lang != default_lang) {  // there is a lang prefix to remove
	var page_part = document.URL.split('/').slice(4).join('/');
} else {  // default_lang has no lang prefix
	var page_part = document.URL.split('/').slice(3).join('/');
}

if (document.referrer.indexOf(dom_part) != 0 && page_lang != nav_lang) {
	var lang_link = dom_part;

	if (nav_lang != default_lang)
		lang_link += '/'+nav_lang;

	if (page_part)
		lang_link += '/'+page_part;

	var lang_node = document.querySelector('a[href="'+lang_link+'"]');

	if (lang_node)
		lang_node.click();
}
