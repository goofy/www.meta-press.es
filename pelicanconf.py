#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Simon Descarpentries'
SITENAME = u'Meta-Press.es'
SITESUBTITLE = u'Decentralized search engine & automatized press reviews'
DESCRIPTION = u'Meta-Press.es : decentralized search engine and automatized press reviews'
SITEURL = 'https://www.meta-press.es'
PATH = 'content'
TIMEZONE = 'Europe/Paris'
DEFAULT_PAGINATION = 5
FEED_ALL_ATOM = 'flux/all.atom.xml'  # Feed generation is usually not desired when developing
# SOCIAL_WIDGET_NAME = 'Flux'
# SOCIAL = (('envelope', 'mailto:siltaar@meta-press.es'), )
RELATIVE_URLS = True  # document-relative URLs when developing, see publishconf.py
PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['asciidoc_reader', 'i18n_subsites', 'tipue_search']
# SLUGIFY_SOURCE = 'basename'
CACHE_CONTENT = True
THEME = 'theme/pelican-mp'
# DISPLAY_CATEGORIES_ON_MENU = False
# DISPLAY_PAGES_ON_MENU = False
MENUITEMS = (
	(u'Index', '/'),
	(u'Journal', '/category/journal.html'),
	(u'A propos', '/pages/a_propos.html'))
LINKS = (
	# (u'Install in Firefox', 'https://'),
	(u'Sources via Framagit', 'https://framagit.org/search?utf8=%E2%9C%93&search=meta-press&'
		'group_id=&project_id=&repository_ref=', '/theme/img/framagitlab_24.png'),
	(u'Support via Liberapay', 'https://liberapay.com/Meta-Press.es',
		'/theme/img/liberapay_16.png'),
)
I18N_SUBSITES = {  # mapping: language_code -> settings_overrides_dict
	'fr': {
		'SITESUBTITLE': u'Moteur de recherche décentralisé & revue de presse automatisée',
	}
}
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
TAG_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
DIRECT_TEMPLATES = ('index', 'categories', 'archives', 'search', 'tipue_search')
TIPUE_SEARCH_SAVE_AS = 'tipue_search.json'
FAVICON = '/theme/img/logo-metapress-32px-bg_.png'
ARTICLE_URL = '{category}/{date:%Y}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{date:%Y}/{slug}.html'
